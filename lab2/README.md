﻿# Questions for consideration
## 1. What security risks can storing passwords in plain text cause? How can this problem be addressed programmatically?
Easy to access, programmer will unintendedly see user passwords. Store passwords in a database.  
## 2. As http requests and responses are text-based information, they can be easily intercepted and read by a third-party on the Internet. Please explain how this problem has been solved in real-world scenarios.  
Each logined user get a user token. Can add extra layer of protection by encoding/salting the message depending on the header-info and the accesss-token. 
##3. How can we use Flask for implementing multi-page web applications? Please explain how Flask templates can help us on the way?  
Flask is a useful tool for...
## 4. Please describe a Database Management System. How SQLite is different from other DBMSs?  
A software for creating and managing databases. Let the programmer create, retrieve, update and manage data.
## 5. Do you think the Telnet client is a good tool for testing server-side procedures? What are its possible shortages?  
We used postman!
