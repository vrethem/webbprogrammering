from flask import Flask
from flask import jsonify
from flask import request
from flask import json
from database_helper import *
import random


app = Flask(__name__)
app.debug = True
users = dict()
X = 6


def create_json(status, mess, data):
    bool = True
    if status >= 400:
        bool = False
    jsonObject = {'success':bool,'message':mess,'data':data}
    return json.dumps(jsonObject)


def create_response(status, mess, data=[]):
    try:
        response = app.response_class(
        response=create_json(status, mess, data),
        status=status,
        mimetype='application/json')
    except Exception as e:
        print (e.message, e.args)
        response = app.response_class(
        response=create_json(False, e.message, e.args),
        status=500,
        mimetype='application/json')
    return response


@app.route('/sign_in',methods=['POST'])
def sign_in():
    print 'sign_in()'
    input = request.json
    if not all (k in input for k in ('password','email')):
        return create_response(400, "Missing input")
    if input["email"] in users.values():
        for token in users:
            if users[token] == input["email"]:
                return create_json(403, "Token already exist for that user", token)
            return create_json(500, 'sign_in() Internal Error ZXCSFDGSTS')
    if find_user(input["email"], input["password"]) is not None:
        letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        token = ""
        for i in range(35):
            token += letters[random.randint(0,len(letters)-1)]
        print 'Returning ' + token
        users[token] = input["email"]
        return create_response(201, "User signed in", token)
    return create_response(400, "User not found")


@app.route('/sign_up',methods=['POST'])
def sign_up():
    print 'sign_up()'
    input = request.json
    if not all (k in input for k in ('password','email','firstname','familyname','gender','city','country')):
        return create_response(400, "Missing input")
    if find_user(input["email"]) is not None:
        return create_response(400, "User already exists")
    if (len(input["password"]) < X):
        return create_response(400, "Password too short")
    if not '@' in input["email"]:
        return create_response(400, "Inavlid email")
    return create_response(201, "User created", add_user(
                   input["email"],
                   input["password"],
                   input["firstname"],
                   input["familyname"],
                   input["gender"],
                   input["city"],
                   input["country"]))


@app.route('/sign_out', methods=['DELETE'])
def sign_out():
    print 'sign_out()'
    token = request.headers.get('Authorization')
    if token == None:
        return create_response(400, 'Not logged in')
    elif token in users:
        del users[token]
        return create_response(201, 'User signed out')
    else:
        return create_response(400, 'Token does not exist')


@app.route('/change_password', methods=['POST'])
def change_password():
    print 'change_password()'
    token = request.headers.get('Authorization')
    if token not in users:
        return create_response(400, 'Not logged in')
    if request.json["oldPassword"] == request.json["newPassword"]:
        return create_response(400, 'New password same as old password')
    if len(request.json["newPassword"]) < X:
        return create_response(400, 'New password too short')
    if find_user(users[token], request.json["oldPassword"]) is None:
        return create_response(403, 'Email or password did not match with databse')

    if change_pwd(users[token], request.json["newPassword"]):
        return create_response(200, 'Password changed')

    return create_response(500, 'Internal server error')


@app.route('/post_message', methods=['POST'])
def post_message():
    print 'post_message()'
    input = request.json
    if not 'message' in request.json or not 'toUser' in request.json:
        return create_response(400, "Missing input")
    if len(str(input["message"])) < 1:
        return create_response(400, "Missing message")

    token = request.headers.get('Authorization')
    if not token in users:
        return create_response(400, 'Not logged in')
    if create_post(input["message"],users[token],input["toUser"]) is not None:
        return create_response(201, 'Messages posted')

    return create_response(500, 'Internal error')


# Exempel usage: http://127.0.0.1:5000/get_messages_byemail?email=some@mail.com
@app.route('/get_messages_byemail', methods=['GET'])
def get_messages_byemail():
    print 'get_messages_byemail() with args: ' + str(request.args)
    if 'email' not in request.args:
        return create_response(400, 'Missing Input')

    token = request.headers.get('Authorization')
    if not token in users:
        return create_response(400, 'Not logged in')

    messages = get_messages(request.args["email"])
    if messages:
        return create_response(201, 'Message retrieved', messages)
    return create_response(500, 'Server Error Or No messages on wall')


@app.route('/get_data_by_email', methods=['GET'])
def get_data_by_email():
    print 'get_data_by_email()'
    if 'email' not in request.args:
        return create_response(400, 'Missing Input')

    token = request.headers.get('Authorization')
    if not token in users:
        return create_response(400, 'Not logged in')

    data = get_user_data(request.args["email"])
    if data:
        return create_response(201, 'Message retrieved',data)
    return create_response(500, 'Server Error: get_data_bemail()')


@app.route('/get_messages_by_token', methods=['GET'])
def get_messages_by_token():
    print 'get_messages_by_token()'
    token = request.headers.get('Authorization')

    if not token in users:
        return create_response(400, 'Not logged in')

    messages = get_messages(users[token])
    if messages:
        return create_response(201, 'Message retrieved',messages)
    return create_response(500, 'Server Error: get_messages_by_token()')


@app.route('/get_data_by_token', methods=['GET'])
def get_data_by_token():
    print 'get_data_by_token()'
    token = request.headers.get('Authorization')

    if not token in users:
        return create_response(400, 'Not logged in')

    data = get_user_data(users[token])
    if data:
        return create_response(201, 'Data retrieved',data)
    return create_response(500,'Server Error')

if __name__ == "__main__":
    app.run()
