1. Let the client do more computations. Performance on client side. Security on server side.
2. Secure, the token is valid until the user logout.
    Can post shit on your wall, cannot change password.
3. Nothing happens. Altough it should be possible.
4. The page is refresh and this is not the behaviour we're looking for.
    Display a warning for example: 'window.onbeforeunload = function() { return "You will  leave this page"; };
'
5. The page resets, see 4
6. You require additional quaries to the server if the view is stored localy.
The extra views will however need to be loaded making the first request large.
7. It is good to know what actually went wrong.
Some sort of message would be good to send back.
8. Data validation is important for client side since it can prevent
 unecessary server quaries and get the user a faster response if they input something wrong.
9. Its not the intended purpose for the html tag. A replacement could be display flex.
10. Already answered in 6.
