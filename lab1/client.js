displayView = function(){
// the code required to display a view

};

window.onload = function(){
//code that is executed as the page is loaded.
//You shall put your own custom code here.
//window.alert() is not allowed to be used in your implementation.
if(localStorage.getItem("loggedin") != null)
{
  var token = JSON.parse(localStorage.getItem("loggedin"));
  var user = serverstub.getUserDataByToken(token);
  console.log(user);
  if(user.success == true)
  {
    //Replace html body with profileview
    document.body.innerHTML = document.getElementById("profileview").textContent;
    //Replace css link to profileview css
    var oldlink = document.getElementsByTagName("link").item(0);
    var csref = document.createElement("link");
    csref.setAttribute("rel","stylesheet");
    csref.setAttribute("type","text/css");
    csref.setAttribute("href","profile.css")
    document.getElementsByTagName("head").item(0).replaceChild(csref, oldlink);
    //Add userinfo to html view
    var userinfo = document.getElementById("userinfo").children;
    user = user.data;
    userinfo[0].textContent = user.email;
    userinfo[1].textContent = user.firstname;
    userinfo[2].textContent = user.familyname;
    userinfo[3].textContent = user.city;
    userinfo[4].textContent = user.country;
    userinfo[5].textContent = user.gender;
    document.getElementById("panel").children[0].className = "active";
    document.getElementById("tab-content").children[0].style.display = "block";

    loadMyMessages();
  }
  else
  {
    document.body.innerHTML = document.getElementById("welcomeview").textContent;
  }
}
else
{
  document.body.innerHTML = document.getElementById("welcomeview").textContent;
}
};

loadMyMessages = function(){
  var token = JSON.parse(localStorage.getItem("loggedin"));
  var obj = serverstub.getUserMessagesByToken(token);
  console.log(obj);

  fillWall("thewall",obj);
};

loadOthersMessages = function(){
  var token = JSON.parse(localStorage.getItem("loggedin"));
  var toEmail = document.getElementsByName("email")[0].value;
  localStorage.setItem("otherwall", JSON.stringify(toEmail));
  var obj = serverstub.getUserMessagesByEmail(token,toEmail);
  document.getElementsByName("email")[0].value = "";

  if(obj.success == true)
  {
    var toShow = document.getElementsByClassName("hidden");
    console.log(toShow);
    if(toShow[0]){
      toShow[0].className = toShow[0].className.replace("hidden","");
      toShow[0].className = toShow[0].className.replace("hidden","");
    }
  }

  fillWall("somewall",obj);
};

fillWall = function(wallId,obj){
  if(obj.success != true)
  {
    showalert(obj.message);
    return;
  }

  var theWall = document.getElementById(wallId);
  theWall.innerHTML = "";
  /*while(theWall.firstChild){
    theWall.removeChild(thewall.firstChild);
  }*/
  console.log(obj.message);
  if(obj.success == true)
  {
    var data = obj.data;
    for(var i = 0; i < data.length; i++)
    {
      var toadd = document.createElement("div");
      var writer = document.createElement("span");
      var message = document.createElement("span");
      writer.style.float = "right";
      writer.style.color = "blue";
      writer.textContent = data[i].writer;
      message.textContent = data[i].content;
      toadd.appendChild(writer);
      toadd.appendChild(message);
      theWall.appendChild(toadd);
    }
  }
};

postmsg = function(){
  var token = JSON.parse(localStorage.getItem("loggedin"));
  var result = serverstub.postMessage(token,
    document.getElementsByName("message")[0].value,
    document.getElementById("userinfo").children[0].textContent);
  console.log(result.message);
  document.getElementsByName("message")[0].value = "";
};

postmsgother = function(){
  var token = JSON.parse(localStorage.getItem("loggedin"));
  var result = serverstub.postMessage(token,
    document.getElementsByName("message2")[0].value,
    JSON.parse(localStorage.getItem("otherwall")));
  document.getElementsByName("message2")[0].value = "";
};

signup = function(){
  //Get input
  var password1 = document.getElementsByName('password2')[0].value;
  var password2 = document.getElementsByName('password3')[0].value;

  //Sanity check
  if(password1.length < 6 || password2.length < 6)
  {
    showalert("Password is too short");
    return;
  }
  if(password1 != password2)
  {
    showalert("Password did not match");
    return;
  }


  //Try to add user
  var result = serverstub.signUp(
    {
      email:document.getElementsByName('email2')[0].value,
      password:password1,
      firstname:document.getElementsByName('firstname')[0].value,
      familyname:document.getElementsByName('familyname')[0].value,
      gender:document.getElementsByName('gender')[0].value,
      city:document.getElementsByName('city')[0].value,
      country:document.getElementsByName('country')[0].value
    });
    console.log(result);
    if(result.success == false){
      showalert(result.message);
    }else{
      var result2 = serverstub.signIn(document.getElementsByName('email2')[0].value,password1);
      localStorage.setItem("loggedin", JSON.stringify(result2.data));
      showalert("User Created");
    }

};

login = function(){
  //Get input
  var email = document.getElementsByName('email2')[0].value;
  var password = document.getElementsByName('password1')[0].value;

  //Sanity check
  if(password.length < 6)
  {
    showalert("Password is too short");
  }

  //Try to log in
  var result = serverstub.signIn(email,password);
  console.log(result);

  if(result.success == true)
  {
    localStorage.setItem("loggedin", JSON.stringify(result.data));
    window.onload();
  }
  else
  {
    showalert(result.message);
  }
};

logout = function(){
  serverstub.signOut(JSON.parse(localStorage.getItem("loggedin")));
};

showalert = function(message){
  var dimmer = document.createElement("div");
  var alertbox = document.createElement("div");
  var form = document.createElement("form");
  var formdiv = document.createElement("div");
  var span = document.createElement("span");
  var divdiv = document.createElement("div");
  var button = document.createElement("button");
  dimmer.id = "dimmer";
  alertbox.id = "alertbox";
  span.id = "alerttext";
  button.setAttribute("onclick","closealert(); return false;");
  dimmer.appendChild(alertbox);
  alertbox.appendChild(form);
  form.appendChild(divdiv);
  formdiv.appendChild(span);
  form.appendChild(formdiv);
  divdiv.appendChild(button);
  span.innerText = message;
  button.innerText = "OK";
  document.body.appendChild(dimmer);
  /*document.body.innerHTML += document.getElementById("alertwindow").textContent;
  document.getElementById("alerttext").innerHTML = message;*/
};

closealert = function(){
  var element = document.getElementById("dimmer");
  element.parentNode.removeChild(element);
};



// profile.js
activeTab = function(evt){
  var tabs = document.getElementById("panel").children;
  var tabcontent = document.getElementById("tab-content").children;

  for(var i=0;i<tabs.length- 1;i++){
    tabs[i].className = "";
    tabcontent[i].style.display = "none";
    if(tabs[i] === evt.currentTarget)
    {
      tabcontent[i].style.display = "block";
    }
  }
  evt.currentTarget.className = " active";
};

changePass = function(){
  var oldpas = document.getElementById("pas1");
  var newpas1 = document.getElementById("pas2");
  var newpas2 = document.getElementById("pas3");

  if(newpas1.value.length < 6){
    showalert("Password too short");
    return;
  }

  if(newpas1.value != newpas2.value){
    showalert("Password does not match");
    return;
  }

  var result = serverstub.changePassword(JSON.parse(localStorage.getItem("loggedin")),oldpas.value,newpas1.value);
  showalert(result.message);
};
