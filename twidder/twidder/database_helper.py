import sqlite3
import logging # Nice logging lib yaaah


def find_user(email,password = None):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    toRet = None
    try:
        if password:
            cur.execute('SELECT * FROM users WHERE email=? AND password=?', (email, password,))
        else:
            cur.execute('SELECT * FROM users WHERE email=?', (email,))
        toRet = cur.fetchone()
    except sqlite3.Error as e:
        print(e)
        toRet = None
    finally:
        cur.close()
        db.close()
        return toRet

def add_user(email,password,firstname,familyname,gender,city,country):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    try:
        cur.execute('INSERT INTO users VALUES (?,?,?,?,?,?,?,?)', (email,password,firstname,familyname,gender,city,country,'default.png',))
        toRet = 'Success'
    except sqlite3.Error as e:
        print(e)
        toRet = None
    finally:
        db.commit()
        cur.close()
        db.close()
        return toRet

def remove_user(email):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    try:
        deletedUser = find_user(self,email)
        cur.execute('DELETE FROM users WHERE email=?', (email,))
    except sqlite3.Error as e:
        print(e)
        deletedUser = None
    finally:
        db.commit()
        cur.close()
        db.close()
        return deletedUser

def change_pwd(email, pwd):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    try:
        cur.execute('UPDATE users SET password=? WHERE email=?', (pwd, email,))
        toRet = 'Success'
    except sqlite3.Error as e:
        print(e)
        toRet = None
    finally:
        db.commit()
        cur.close()
        db.close()
        return toRet

def change_img(email,filename):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    try:
        cur.execute('UPDATE users SET profilepic=? WHERE email=?', (filename, email,))
        toRet = 'Success'
    except sqlite3.Error as e:
        print(e)
        toRet = None
    finally:
        db.commit()
        cur.close()
        db.close()
        return toRet

def create_post(message, fromUser, toUser,type):
    db = sqlite3.connect('database.db')
    cur = db.cursor()
    toRet = None
    try:
        cur.execute('INSERT INTO messages VALUES (?,?,?,?)', (message,fromUser,toUser,type,))
        toRet = message
    except sqlite3.Error as e:
        print e
    finally:
        db.commit()
        cur.close()
        db.close()
        return toRet

def get_messages(email):
    db = sqlite3.connect('database.db')
    db.row_factory = sqlite3.Row
    cur = db.cursor()
    toRet = None
    try:
        cur.execute('SELECT message,fromUser,type FROM messages WHERE toUser=?', (email,))
        toRet = [dict(row) for row in cur.fetchall()]
        if not toRet:
            toRet = ' '
    except sqlite3.Error as e:
        print e
    finally:
        db.commit()
        cur.close()
        db.close()
        return toRet

def get_user_data(email):
    db = sqlite3.connect('database.db')
    db.row_factory = sqlite3.Row
    cur = db.cursor()
    try:
        cur.execute('SELECT email,firstname,familyname,male,city,country,profilepic \
            FROM users WHERE email=?', (email,))
        toRet = [dict(row) for row in cur.fetchall()]
        if not toRet:
            toRet = ' '
    except sqlite3.Error as e:
        console.error(e)
    finally:
        db.commit()
        cur.close()
        db.close()
        return toRet

if __name__ == '__main__':
    print 'started'
