OurWebSocketService = {

  openWebSocket: function (token) {
    if ("WebSocket" in window) {

       // Open a web socket
       ws = new WebSocket("ws://localhost:5000/api");
       console.log(ws);

       ws.onopen = function() {
         // Web Socket is connected
         ws.send(token);
          console.log("ws is open");
       };

       ws.onmessage = function (evt) {
         console.log("recievedmsg: ",evt);
          var received_msg = evt.data;
          if(received_msg == "signout"){
              logout();
              ws.close();
          }
       };

       ws.onclose = function() {
          console.log("ws is closed");
          // websocket is closed.
       };
    } else {
       console.log("The browser doesn't support WebSocket");
    }
  },

  closeWebSocket: function(){
    ws.close();
  }
};

OurWebSocketService.ws = 0;
