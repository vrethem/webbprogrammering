//Switches the active view to logged in view
switchToProfileView = async function(user){
  //Replace html body with profileview
  document.body.innerHTML = document.getElementById("profileview").textContent;
  //Change css document to profileview css
  var oldlink = document.getElementsByTagName("link").item(0);
  var csref = document.createElement("link");
  csref.setAttribute("rel", "stylesheet");
  csref.setAttribute("type", "text/css");
  csref.setAttribute("href", "profile.css")
  document.getElementsByTagName("head").item(0).replaceChild(csref, oldlink);
  //Add userdata to the view
  var userinfo = document.getElementById("userinfo").children;
  user = user.data[0];
  userinfo[0].textContent = user.email;
  userinfo[1].textContent = user.firstname;
  userinfo[2].textContent = user.familyname;
  userinfo[3].textContent = user.city;
  userinfo[4].textContent = user.country;
  if(user.male == "1")
    userinfo[5].textContent = "Male";
  else
    userinfo[5].textContent = "Female";
  document.getElementById('userimage').children[0].src = await getImageUrl(user.profilepic);
  //Make the first tab of the view the active tab
  document.getElementById("panel").children[0].className = "active";
  document.getElementById("tab-content").children[0].style.display = "block";
}

//Switches the active view to welcomeview
switchToWelcomeView = function(){
  //Replace html body with profileview
  document.body.innerHTML = document.getElementById("welcomeview").textContent;
  //Change css document to welcomeview css
  var oldlink = document.getElementsByTagName("link").item(0);
  var csref = document.createElement("link");
  csref.setAttribute("rel", "stylesheet");
  csref.setAttribute("type", "text/css");
  csref.setAttribute("href", "client.css")
  document.getElementsByTagName("head").item(0).replaceChild(csref, oldlink);
}

getImageUrl = async function(filename){
  blob = await serverstub.getImage(filename);
  toRet = "imageNotFound.png"
  try{
    toRet = URL.createObjectURL(blob);
  }catch (e){
    console.log("Failed to load image: ",e);
  }
  return toRet;
}

/* depricated, see comment in serverstub.getVideo 
getVideoUrl = async function(filename){
  blob = await serverstub.getVideo(filename);
  try{
    toRet = URL.createObjectURL(blob);
  }catch (e){
    console.log("Failed to load video: ",e);
  }
  return toRet;
}*/

//Run when the html document finished loading
window.onload = async function () {
  //Check for any localy stored session tokens
  if (localStorage.getItem("loggedin") != null) {
    var token = localStorage.getItem("loggedin");

    // Try to get the userdata from server
    var user = await serverstub.getUserDataByToken(token);
    console.log('<Windows.onLoad()> Get user ', user);

    if (user.success == true) {
      switchToProfileView(user);
      loadMyMessages(token);
      OurWebSocketService.openWebSocket(token);
      return;
    }
    else{
      localStorage.removeItem("loggedin");
    }
  }
  //If we didnt manage to use a lasting session
  //Show the welcomeview instead
  switchToWelcomeView();
};

//Gets the messages on the active users wall and puts them into view
loadMyMessages = async function (token) {
  var obj = await serverstub.getUserMessagesByToken(token);
  fillWall("thewall", obj);
};

//Gets the messages of some other user and puts them into view
loadOthersMessages = async function (elements) {
  //Get our token and which users wall we wish to see
  var token = localStorage.getItem("loggedin")
  var toEmail = elements.namedItem("email").value;
  //Now we save the email of the loaded wall if one wishes to add a message \
  //to the wall of the user they loaded
  localStorage.setItem("postReciever", JSON.stringify(toEmail));
  var obj = await serverstub.getUserMessagesByEmail(token, toEmail);
  elements.namedItem("email").value = "";

  if (obj.success == true) {
    //If we managed to load a wall, show the submit message button and wall
    //elements in the html view
    //The get elementsbyclassname returns a "live" list
    //This means if we remove a classname the list removes the jsonObject
    //Therefore a while loop is nessecary
    var toShow = document.getElementsByClassName("hidden");
    while(toShow.length){
      if (toShow[0].classList.contains('hidden'))
        toShow[0].classList.remove('hidden');
    }
  }
  fillWall("somewall", obj);
};

//Generic function for creating clean looking post elements in the view
//They will be created under wallId given with the content from obj
fillWall = async function (wallId, obj) {
  if (obj.success != true) {
    showalert(obj.message);
    return;
  }

  var theWall = document.getElementById(wallId);
  theWall.innerHTML = "";
  while(theWall.firstChild){
    theWall.removeChild(thewall.firstChild);
  }
    var data = obj.data;
    for (var i = 0; i < data.length; i++) {
      switch(data[i].type){
        case 'text':
          var toadd = document.createElement("div");
          var writer = document.createElement("span");
          var message = document.createElement("span");
          writer.style.float = "right";
          writer.style.color = "blue";
          writer.textContent = data[i].fromUser;
          message.textContent = data[i].message;
          toadd.appendChild(writer);
          toadd.appendChild(message);
          theWall.appendChild(toadd);
          break;
        case 'image':
          var toadd = document.createElement("div");
          var writer = document.createElement("span");
          var message = document.createElement("img");
          writer.style.float = "right";
          writer.style.color = "blue";
          writer.textContent = data[i].fromUser;
          message.src = await getImageUrl(data[i].message);
          message.style.width = "400px";
          toadd.appendChild(writer);
          toadd.appendChild(message);
          theWall.appendChild(toadd);
          break;
        case 'video':
          var toadd = document.createElement("div");
          var writer = document.createElement("span");
          var message = document.createElement("video");
          writer.style.float = "right";
          writer.style.color = "blue";
          writer.textContent = data[i].fromUser;
          message.src = 'http://127.0.0.1:5000/get_media_stream?name=' + data[i].message; //await getVideoUrl(data[i].message);
          message.width = "400";
          message.setAttribute("controls","");
          toadd.appendChild(writer);
          toadd.appendChild(message);
          theWall.appendChild(toadd);
          break;
      }
    }
};

// Posts a message containing media to active users own wall
postmedia = async function (elements) {
  var token = localStorage.getItem("loggedin")
  var formData = new FormData();
  formData.set("file", elements.namedItem("file").files[0]);
  var result = await serverstub.postMedia(token,formData,document.getElementById("userinfo").children[0].textContent);
    showalert(result.message);
};

//Posts a message to active users own wall
postmsg = async function (elements) {
  var token = localStorage.getItem("loggedin")
  var result = await serverstub.postMessage(token,
    elements.namedItem("message").value,
    document.getElementById("userinfo").children[0].textContent);
  elements.namedItem("message").value = "";
    showalert(result.message);
};

//Posts a message containing media to some other users wall
//The reciving users email is put in storage when their wall is loaded
postmediaother = async function (elements) {
  var token = localStorage.getItem("loggedin")
  var formData = new FormData();
  formData.set("file", elements.namedItem("file").files[0]);
  var result = await serverstub.postMedia(token,formData,JSON.parse(localStorage.getItem("postReciever")));
  showalert(result.message);
};

//Posts a message to some other users wall
//The reciving users email is put in storage when their wall is loaded
postmsgother = async function (elements) {
  var token = localStorage.getItem("loggedin")
  var result = await serverstub.postMessage(token,
    elements.namedItem("message").value,
    JSON.parse(localStorage.getItem("postReciever")));
  elements.namedItem("message2").value = "";
    showalert(result.message);
};

//Attempt to create a new user
signup = async function (elements) {
  //Get input
  var email = elements.namedItem('email').value;
  var firstname = elements.namedItem('firstname').value;
  var familyname = elements.namedItem('familyname').value;
  var gender = elements.namedItem('gender').value;
  var city = elements.namedItem('city').value;
  var country = elements.namedItem('country').value;
  var password1 = elements.namedItem('password1').value;
  var password2 = elements.namedItem('password2').value;

  if(gender == "male")
    gender = "1";
  else
    gender = "0";

  if (password1.length < 6 || password2.length < 6) {
    showalert("Password is too short");
    return;
  }
  if (password1 != password2) {
    showalert("Password did not match");
    return;
  }
  //Try to add user
  var result = await serverstub.signUp(
    {
      email: email,
      password: password1,
      firstname: firstname,
      familyname: familyname,
      gender: gender,
      city: city,
      country: country
    });
  console.log(result);
  showalert(result.message);
};

login = async function (elements) {
  //Get input values
  var email = elements.namedItem('email').value;
  var password = elements.namedItem('password').value;
  //Clientside input validation
  if (password.length < 6 || email.indexOf('@') == -1) {
    showalert("Bad user input");
    return;
  }
  //Try to signin with server
  result = await serverstub.signIn(email, password);
  if (result.success  == true) {
      localStorage.setItem("loggedin", result.data["token"]);
      window.onload();
  }
   else {
    showalert(result.message);
  }
};

logout = function () {
   serverstub.signOut(localStorage.getItem("loggedin"));
   localStorage.removeItem("loggedin");
   OurWebSocketService.closeWebSocket();
   window.onload();
};

//Homemade window alert
showalert = function (message) {
  var dimmer = document.createElement("div");
  var alertbox = document.createElement("div");
  var form = document.createElement("form");
  var formdiv = document.createElement("div");
  var span = document.createElement("span");
  var divdiv = document.createElement("div");
  var button = document.createElement("button");
  dimmer.id = "dimmer";
  alertbox.id = "alertbox";
  span.id = "alerttext";
  button.setAttribute("onclick", "closealert(); return false;");
  dimmer.appendChild(alertbox);
  alertbox.appendChild(form);
  formdiv.appendChild(span);
  form.appendChild(formdiv);
  form.appendChild(divdiv);
  divdiv.appendChild(button);
  span.innerText = message;
  button.innerText = "OK";
  document.body.appendChild(dimmer);
};

closealert = function () {
  var element = document.getElementById("dimmer");
  element.parentNode.removeChild(element);
};

activeTab = function (evt) {
  var tabs = document.getElementById("panel").children;
  var tabcontent = document.getElementById("tab-content").children;

  for (var i = 0; i < tabs.length - 1; i++) {
    tabs[i].className = "";
    tabcontent[i].style.display = "none";
    if (tabs[i] === evt.currentTarget) {
      tabcontent[i].style.display = "block";
    }
  }
  evt.currentTarget.className = " active";
};

changePass = async function (elements) {
  var oldpas = elements.namedItem("pas1");
  var newpas1 = elements.namedItem("pas2");
  var newpas2 = elements.namedItem("pas3");

  if (newpas1.value.length < 6) {
    showalert("Password too short");
    return;
  }

  if (newpas1.value != newpas2.value) {
    showalert("Password does not match");
    return;
  }

  var result = await serverstub.changePassword(localStorage.getItem("loggedin"),
                                               oldpas.value,
                                               newpas1.value);
  showalert(result.message);
};

changeImg = async function (elements){
  var token = localStorage.getItem("loggedin")
  var formData = new FormData();
  formData.set("file", elements.namedItem("file").files[0]);
  var result = await serverstub.changeImage(token,formData);
  showalert(result.message);
}
