//Handles all communication attempts with the server
//Functions should return mapobjects with content {sucess:true/false message:string data:optionaldata}

var serverstub = {

  /* Post a XmltHttpRequest to the server
  *  with a callback function that is called upon success
  *   @(jsonObj) - JSON-object to send
  *
  **/
  /** This works but we prefer using promise
 use_callback:  function (method, url, use_async, callback, jsonObj={}) {
    try {
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status < 400) {
          var result = JSON.parse(xmlhttp.response);
          console.log('Client-XmlHttpReq ', result);
          console.log(result["data"])
          return callback(result);
        }
        else if (this.onreadystatechange == 4 && this.status >= 400) {
          showalert(result.message);
        }
        else {
          console.log('Waiting for response...');
        }
      };
      xmlhttp.open(method, url, use_async);
      xmlhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
      if (localStorage.getItem("loggedin") != null) {
        xmlhttp.setRequestHeader('Authorization', localStorage.getItem("loggedin"));
      }
      if (method != 'GET') {
        xmlhttp.send();
      } else {
        xmlhttp.send(JSON.stringify(jsonObj));
      }
    }
    catch (err) {
      console.error('ERROR use_callback():', err);
    }
  },
**/
  /* Post a XmltHttpRequest to the server
  *  with a a promise
  *  Needs to be used with .then(...)
  *                        .catch(...)
  *   @(jsonObj) - JSON-object to send
  *
  **/
  use_promise: function (method, url, use_async, jsonObj = {}) {
    return new Promise(function (resolve, reject) {
      const xmlhttp = new XMLHttpRequest();
      if (use_async) {
        xmlhttp.timeout = 2000;
      }
      xmlhttp.onreadystatechange = function (e) {
        if (xmlhttp.readyState === 4) {
          if (xmlhttp.status < 400) {
            resolve(xmlhttp.response);
          } else {
            reject(xmlhttp.response)
          }
        }
      }
      if (use_async) {
        xmlhttp.ontimeout = function () {
          reject('timeout')
        }
      }
      xmlhttp.open(method, url, use_async);
      xmlhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
      if (localStorage.getItem("loggedin") != null) {
        xmlhttp.setRequestHeader('Authorization', localStorage.getItem("loggedin"));
      }
      if (method === 'GET') {
        xmlhttp.send();
      } else {
        xmlhttp.send(JSON.stringify(jsonObj));
      }
    });
  },

  use_promise_get_file: function (method, url, use_async, jsonObj = {}) {
    return new Promise(function (resolve, reject) {
      const xmlhttp = new XMLHttpRequest();
      if (use_async) {
        xmlhttp.timeout = 2000;
      }
      xmlhttp.onreadystatechange = function (e) {
        if (xmlhttp.readyState === 4) {
          if (xmlhttp.status < 400) {
            resolve(xmlhttp.response);
          } else {
            reject(xmlhttp.response)
          }
        }
      }
      if (use_async) {
        xmlhttp.ontimeout = function () {
          reject('timeout')
        }
      }
      xmlhttp.open(method, url, use_async);
      xmlhttp.responseType = "arraybuffer";
      xmlhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
      if (localStorage.getItem("loggedin") != null) {
        xmlhttp.setRequestHeader('Authorization', localStorage.getItem("loggedin"));
      }
      if (method === 'GET') {
        xmlhttp.send();
      } else {
        xmlhttp.send(JSON.stringify(jsonObj));
      }
    });
  },

  use_promise_get_stream: function (method, url, use_async, jsonObj = {}) {
    return new Promise(function (resolve, reject) {
      const xmlhttp = new XMLHttpRequest();
      if (use_async) {
        xmlhttp.timeout = 2000;
      }
      xmlhttp.onreadystatechange = function (e) {
        if (xmlhttp.readyState === 4) {
          if (xmlhttp.status < 400) {
            resolve(xmlhttp.response);
          } else {
            reject(xmlhttp.response)
          }
        }
      }
      if (use_async) {
        xmlhttp.ontimeout = function () {
          reject('timeout')
        }
      }
      xmlhttp.open(method, url, use_async);
      xmlhttp.responseType = "multipart/byterange";
      //xmlhttp.setRequestHeader('Range', 'bytes=0-1023'); Set automatically
      xmlhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');
      if (localStorage.getItem("loggedin") != null) {
        xmlhttp.setRequestHeader('Authorization', localStorage.getItem("loggedin"));
      }
      if (method === 'GET') {
        xmlhttp.send();
      } else {
        xmlhttp.send(JSON.stringify(jsonObj));
      }
    });
  },

  use_promise_send_file: function (method, url, use_async, jsonObj = {}, toEmail) {
    return new Promise(function (resolve, reject) {
      const xmlhttp = new XMLHttpRequest();
      if (use_async) {
        xmlhttp.timeout = 2000;
      }
      xmlhttp.onreadystatechange = function (e) {
        if (xmlhttp.readyState === 4) {
          if (xmlhttp.status < 400) {
            resolve(xmlhttp.response);
          } else {
            reject(xmlhttp.response)
          }
        }
      }
      if (use_async) {
        xmlhttp.ontimeout = function () {
          reject('timeout')
        }
      }
      xmlhttp.open(method, url, use_async);
      if (localStorage.getItem("loggedin") != null) {
        xmlhttp.setRequestHeader('Authorization', localStorage.getItem("loggedin"));
      }
      if  (toEmail != null){
        xmlhttp.setRequestHeader('toEmail',toEmail);
      }
      if (method === 'GET') {
        xmlhttp.send();
      } else {
        xmlhttp.send(jsonObj);
      }
    });
  },


  /* Post a XmltHttpRequest to the server
  *  with a callback function that is called upon success
  *   @(jsonObj) - JSON-object to send
  *
  **/
  signIn: function (email, password) {
    return serverstub.use_promise("POST", "sign_in", true, { "email": email, "password": password })
    .then( function (result) {
      result = JSON.parse(result);
      console.log('signIn(): ', result);
      return result;
    })
    .catch(function (error) {
      console.log('<ERROR> signIn(): ', error);
      try {
        error = JSON.parse(error);
      } catch (e) {
        return { "message":"Could not understand server response", "success":false}
      }
      return error;
    });
  },

/*
Can replace above if callback is used
  signIn: async function (email, password) {
    res = 'Waiting';
    console.log('signIn(): ', res);
    var res = await serverstub.use_callback({ "email": email, "password": password },
      function (result) {
        console.log('signIn():', result);
        if (responseData["token"]) {
          localStorage.setItem("loggedin", responseData["token"]); // Rename to token
        }
        window.onload();
      });
    },
*/

  postMessage: function (token, content, toEmail) {
    return serverstub.use_promise("POST", "/post_message", true, { "message": content, "toUser": toEmail })
      .then( result => {
        result = JSON.parse(result);
        console.log('postMessage()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> postMessage(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  postMedia: function (token,fileData, toEmail) {
    return serverstub.use_promise_send_file("POST", "/post_media", true, fileData, toEmail)
      .then( result => {
        result = JSON.parse(result);
        console.log('postMedia()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> postMedia(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  getUserDataByToken: function (token) {
  return serverstub.use_promise("GET", "/get_data_by_token", true)
    .then( result => {
      result = JSON.parse(result);
      console.log('getUserDataByToken()',result);
      return result;
    })
    .catch(function (error) {
      console.log('<ERROR> getUserDataByToken(): ', error);
      try {
        error = JSON.parse(error);
      } catch (e) {
        return { "message":"Could not understand server response", "success":false}
      }
      return error;
    });
  },

  getUserDataByEmail: function (token, email) {
    url = "/get_data_by_email?email="+email;
    return serverstub.use_promise("GET", url, true)
      .then( result => {
        result = JSON.parse(result);
        console.log('getUserDataByEmail()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> getUserDataByEmail(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  getUserMessagesByToken: function (token) {
    return serverstub.use_promise("GET", "/get_messages_by_token", true)
      .then( result => {
        result = JSON.parse(result);
        console.log('getUserMessagesByToken()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> getUserMessagesByToken(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  getUserMessagesByEmail: function (token, email) {
    url = "/get_messages_by_email?email="+email;
    return serverstub.use_promise("GET", url, true)
      .then( result => {
        result = JSON.parse(result);
        console.log('getUserMessagesByEmail()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> getUserMessagesByEmail(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error
      });
  },

  signOut: function (token) {
    return serverstub.use_promise("DELETE", "/sign_out", true)
      .then( result => {
        //result = JSON.parse(result); We dont get a response from the delete?
        console.log('signOut()', result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> signOut(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  signUp: function (inputObject) { // {email, password, firstname, familyname, gender, city, country}
    return serverstub.use_promise("POST", "/sign_up", true, inputObject)
      .then( result => {
        result = JSON.parse(result);
        console.log('signUp()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> signUp(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  getImage: function (filename){
    return serverstub.use_promise_get_file("POST", "/get_media", true, {filename:filename})
      .then( result => {
        console.log('getImage()');
        return new Blob([result],{type:"image/png"});
      })
      .catch(function (error) {
        console.log('<ERROR> getImage(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

/* //Function for getting whole video files, as we stream files this is now depricated
  getVideo: function (filename){
    return serverstub.use_promise_get_stream("POST", "/get_media_stream", true, {filename:filename})
      .then( result => {
        console.log('getFile()');
        return new Blob([result],{type:"video/mp4"});
      })
      .catch(function (error) {
        console.log('<ERROR> getFile(): ', error);
        return error;
      });
  },
*/

  changePassword: function (token, oldPassword, newPassword) {
    return serverstub.use_promise("POST", "/change_password", true, { "oldPassword": oldPassword, "newPassword": newPassword })
      .then( result => {
        result = JSON.parse(result);
        console.log('change_password()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> change_password(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  },

  changeImage: function (token, newImage){
    return serverstub.use_promise_send_file("POST", "/change_image", true, newImage)
      .then( result => {
        result = JSON.parse(result);
        console.log('changeImage()',result);
        return result;
      })
      .catch(function (error) {
        console.log('<ERROR> changeImage(): ', error);
        try {
          error = JSON.parse(error);
        } catch (e) {
          return { "message":"Could not understand server response", "success":false}
        }
        return error;
      });
  }
};
