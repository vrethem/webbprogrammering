from flask import Flask
from flask import jsonify
from flask import request
from flask import json
from flask import send_from_directory, config
from database_helper import *
from videostream_helper import *
import random
import json
import os

app = Flask(__name__, static_url_path='')
app.debug = True
users = dict()
X = 6


def create_json(status, mess, data):
    bool = (status < 400)
    jsonObject = {'success': bool, 'message': mess, 'data': data}
    return json.dumps(jsonObject)


""" Create response
    + content-type is set to application/json thrue mimetype
"""
def create_response(status, mess, data=[]):
    try:
        response = app.response_class(
            response=create_json(status, mess, data),
            status=status,
            mimetype='application/json')
    except Exception as e:
        print(e.message, e.args)
        response = app.response_class(
            response=create_json(False, e.message, e.args),
            status=500,
            mimetype='application/json')
    return response

'''
def create_unique_mediafilename(fileext):
    letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    filename = ""
    while True:
        for i in range(20):
                filename += letters[random.randint(0, len(letters)-1)]
        filename += fileext
        filename = 'twidder/media/' + filename
        if not os.path.isfile(filename):
            break
    return filename
'''

@app.route('/sign_in', methods=['POST'])
def sign_in():
    input = request.json
    try:
        print '<server>/sign_in ', jsonify(input), json.dumps(input)
        if not all(k in input for k in ("password", "email")):
            return create_response(406, "Missing input")

        if find_user(input["email"], input["password"]) is not None:
            letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
            token = ""
            for i in range(35):
                token += letters[random.randint(0, len(letters)-1)]

            users[token] = input["email"]
            print '             Added  users[' + token + '] = ' + users[token]
            return create_response(201, "User signed in", {"token": token})
        return create_response(400, "Username/password not exists")
    except ValueError as e:
        print e.message, e.args
        return create_response(500, "Sign_in python error")


@app.route('/sign_up', methods=['POST'])
def sign_up():
    print 'sign_up()'
    input = request.json
    if not all(k in input for k in ('password', 'email', 'firstname', 'familyname', 'gender', 'city', 'country')):
        return create_response(400, "Missing input")
    if find_user(input["email"]) is not None:
        return create_response(400, "User already exists")
    if (len(input["password"]) < X):
        return create_response(400, "Password too short")
    if not '@' in input["email"]:
        return create_response(400, "Inavlid email")
    if add_user(
        input["email"],
        input["password"],
        input["firstname"],
        input["familyname"],
        input["gender"],
        input["city"],
        input["country"]):
        print '                      User created ', input
        return create_response(201, "User created")
    return create_response(500, "Server error")


@app.route('/sign_out', methods=['DELETE'])
def sign_out():
    print 'sign_out()'
    token = request.headers.get('Authorization')
    if token == None:
        return create_response(400, 'Not logged in')
    elif token in users:
        del users[token]
        return create_response(201, 'User signed out')
    else:
        return create_response(400, 'Token does not exist')


@app.route('/change_password', methods=['POST'])
def change_password():
    print 'change_password()'
    token = request.headers.get('Authorization')
    if token not in users:
        return create_response(400, 'Not logged in')
    if request.json["oldPassword"] == request.json["newPassword"]:
        return create_response(400, 'New password same as old password')
    if len(request.json["newPassword"]) < X:
        return create_response(400, 'New password too short')
    if find_user(users[token], request.json["oldPassword"]) is None:
        return create_response(403, 'Email or password did not match with databse')

    if change_pwd(users[token], request.json["newPassword"]):
        return create_response(200, 'Password changed')

    return create_response(500, 'Internal server error')

@app.route('/change_image', methods=['POST'])
def change_image():
    print 'change_image()'
    token = request.headers.get('Authorization')
    file = request.files.get('file')
    if not token in users:
        return create_response(400, 'Not logged in')
    if file is None:
        return create_response(400, 'Input file missing')
    filepath = ""
    filaname = ""
    fileext = ""
    while True:
        letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        filename = ""
        for i in range(20):
                filename += letters[random.randint(0, len(letters)-1)]
        fileext = os.path.splitext(file.filename)[1]
        filename += fileext
        filepath = 'twidder/media/' + filename
        if not os.path.isfile(filepath):
            break
    file.save(filepath)
    if change_img(users[token],filename) is not None:
        return create_response(201, 'Profile image changed')
    return create_response(500, 'Internal error')

@app.route('/post_message', methods=['POST'])
def post_message():
    print 'post_message()'
    input = request.json
    if not 'message' in request.json or not 'toUser' in request.json:
        return create_response(400, "Missing input")
    if len(str(input["message"])) < 1:
        return create_response(400, "Missing message")

    token = request.headers.get('Authorization')
    if not token in users:
        return create_response(400, 'Not logged in')
    if create_post(input["message"], users[token], input["toUser"],'text') is not None:
        return create_response(201, 'Messages posted')

    return create_response(500, 'Internal error')


# Exempel usage: http://127.0.0.1:5000/get_messages_byemail?email=some@mail.com
@app.route('/get_messages_by_email', methods=['GET'])
def get_messages_byemail():
    print 'get_messages_byemail() with args: ' + str(request.args)
    if 'email' not in request.args:
        return create_response(400, 'Missing Input')

    token = request.headers.get('Authorization')
    if not token in users:
        return create_response(400, 'Not logged in')

    messages = get_messages(request.args["email"])
    if messages:
        return create_response(201, 'Message retrieved', messages)
    return create_response(500, 'Server Error Or No messages on wall')


@app.route('/get_data_by_email', methods=['GET'])
def get_data_by_email():
    print 'get_data_by_email()'
    if 'email' not in request.args:
        return create_response(400, 'Missing Input')

    token = request.headers.get('Authorization')
    if not token in users:
        return create_response(400, 'Not logged in')

    data = get_user_data(request.args["email"])
    if data:
        return create_response(201, 'Message retrieved', data)
    return create_response(500, 'Server Error: get_data_bemail()')


@app.route('/get_messages_by_token', methods=['GET'])
def get_messages_by_token():
    print 'get_messages_by_token()'
    token = request.headers.get('Authorization')

    if not token in users:
        return create_response(400, 'Not logged in')

    messages = get_messages(users[token])

    if messages:
        return create_response(201, 'Message retrieved', messages)
    return create_response(500, 'Server Error: get_messages_by_token()')


@app.route('/get_data_by_token', methods=['GET'])
def get_data_by_token():
    print 'get_data_by_token()'
    token = request.headers.get('Authorization')

    if not token in users:
        return create_response(400, 'Not logged in')

    data = get_user_data(users[token])
    if data:
        return create_response(201, 'Data retrieved', data)
    return create_response(500, 'Server Error')

@app.route('/post_media', methods = ['POST'])
def post_media():
    print 'post_media()'
    token = request.headers.get('Authorization')
    toEmail = request.headers.get('toEmail')
    file = request.files.get('file')
    if not token in users:
        return create_response(400, 'Not logged in')
    if file is None:
        return create_response(400, 'Input file missing')
    if toEmail is None:
        return create_response(400, 'No reciever for post found')
    filepath = ""
    filaname = ""
    fileext = ""
    while True:
        letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        filename = ""
        for i in range(20):
                filename += letters[random.randint(0, len(letters)-1)]
        fileext = os.path.splitext(file.filename)[1]
        filename += fileext
        filepath = 'twidder/media/' + filename
        if not os.path.isfile(filepath):
            break
    file.save(filepath)

    if fileext in ('.png','.jpg','.jpeg','.gif'):
        if create_post(filename,users[token],toEmail,'image') is not None:
            return create_response(201, 'Image recieved')
    elif fileext in ('.mp4'):
        if create_post(filename,users[token],toEmail,'video') is not None:
            return create_response(201, 'Video recieved')
    else:
        return create_response(400, 'Unrecognized filetype')
    return create_response(500, 'Internal error')

@app.route('/get_media', methods = ['POST'])
def get_media():
    print 'get_media()'
    input = request.json
    token = request.headers.get('Authorization')
    if token not in users:
        return create_response(400, 'Not logged in')
    if 'filename' not in input:
        return create_response(400, 'Input parameter missing')
    return send_from_directory('media', input['filename']);

@app.route('/get_media_stream', methods = ['GET'])
def get_media_stream():
    print 'get_media_stream()'
    #input = request.json
    #token = request.headers.get('Authorization')
    #if token not in users:
    #    return create_response(400, 'Not logged in')
    filename = request.args["name"]
#    print 'Filename is: ' + filename
    if not filename:
        return create_response(400, 'Input parameter missing')
    return stream_video('twidder/media/' + filename)

@app.route('/')
def root():
    return app.send_static_file('client.html')


if __name__ == "__main__":
    app.run()
