from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from twidder import app

http_server = WSGIServer(('', 5000), app)
http_server.serve_forever()