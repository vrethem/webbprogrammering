from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer

from flask import Flask, request, render_template
from twidder import app, users


'''
def application(env, start_response):
    # complete the handshake
    pywsgi.websocket_handshake(env['HTTP_SEC_WEBSOCKET_KEY'], env.get('HTTP_ORIGIN', ''))
    while True:
        msg = pywsgi.websocket_recv()
        pywsgi.websocket_send(msg)
'''
wSockets = dict()

@app.route('/api')
def api():
    if request.environ.get('wsgi.websocket'):
        ws = request.environ['wsgi.websocket']
        token = str(ws.receive())
        token = users[token]
        if token in wSockets:
            try:
                print 'Sending signout'
                oldsocket = wSockets[token]
                wSockets[token] = ws
                oldsocket.send('signout')
                oldsocket.close()
            except Exception as e:
                print 'Could not send signout:', e
        else:
            wSockets[token] = ws
        while not ws.closed:
                msg = ws.receive()
                if msg is not None:
                    try:
                        ws.send(msg)
                    except Exception as e:
                        print 'WebSocket Error:', e
        if wSockets[token] == ws:
            del wSockets[token]
    return 'K thx baj'

http_server = WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
http_server.serve_forever()
