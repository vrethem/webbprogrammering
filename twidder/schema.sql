DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS messages;
CREATE TABLE users(email varchar(20) PRIMARY KEY,
                  password varchar(20),
                  firstname varchar(20),
                  familyname varchar(20),
                  male boolean,
                  city varchar(20),
                  country varchar(20),
                  profilepic varchar(30));
CREATE TABLE messages(message varchar(255),
                      fromUser varchar(20),
                      toUser varchar(20),
                      type varchar(5));
INSERT INTO users VALUES('a@a.com','123123', 'Ren', 'Ren',1,'Persona','Japan','default.png');
INSERT INTO messages VALUES('This is a default test message','admin@cool.op','a@a.com','text');
INSERT INTO messages VALUES('testimage1.png','admin@cool.op','a@a.com','image');
INSERT INTO messages VALUES('testimage2.jpg','admin@cool.op','a@a.com','image');
INSERT INTO messages VALUES('testvideo1.mp4','admin@cool.op','a@a.com','video');
